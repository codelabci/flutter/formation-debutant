import 'dart:convert';

class User {
  dynamic id;
  String? firstName;
  String name;
  String phone;
  String? email;
  String password;
  String? profil;
  int isActive;
  dynamic createdAt;

  User({
    required this.id,
    this.firstName,
    required this.name,
    required this.phone,
    this.email,
    required this.password,
    this.profil,
    this.isActive = 0,
    this.createdAt,
  });

  User copyWith({
    dynamic id,
    String? firstName,
    String? name,
    String? phone,
    String? email,
    String? password,
    String? profil,
    int? isActive,
    dynamic createdAt,
  }) {
    return User(
      id: id ?? this.id,
      firstName: firstName ?? this.firstName,
      name: name ?? this.name,
      phone: phone ?? this.phone,
      email: email ?? this.email,
      password: password ?? this.password,
      profil: profil ?? this.profil,
      isActive: isActive ?? this.isActive,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'firstName': firstName,
      'name': name,
      'phone': phone,
      'email': email,
      'password': password,
      'profil': profil,
      'isActive': isActive,
      'createdAt': createdAt,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['id'],
      firstName: map['firstName'],
      name: map['name'] ?? '',
      phone: map['phone'] ?? '',
      email: map['email'],
      password: map['password'] ?? '',
      profil: map['profil'],
      isActive: map['isActive']?.toInt() ?? 0,
      createdAt: map['createdAt'],
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));

  @override
  String toString() {
    return 'User(id: $id, firstName: $firstName, name: $name, phone: $phone, email: $email, password: $password, profil: $profil, isActive: $isActive, createdAt: $createdAt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is User &&
        other.id == id &&
        other.firstName == firstName &&
        other.name == name &&
        other.phone == phone &&
        other.email == email &&
        other.password == password &&
        other.profil == profil &&
        other.isActive == isActive &&
        other.createdAt == createdAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        firstName.hashCode ^
        name.hashCode ^
        phone.hashCode ^
        email.hashCode ^
        password.hashCode ^
        profil.hashCode ^
        isActive.hashCode ^
        createdAt.hashCode;
  }
}
