import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LandingPageWithAnimation extends StatefulWidget {
  const LandingPageWithAnimation({Key? key}) : super(key: key);

  @override
  State<LandingPageWithAnimation> createState() =>
      _LandingPageWithAnimationState();
}

class _LandingPageWithAnimationState extends State<LandingPageWithAnimation>
    with TickerProviderStateMixin {
  late final AnimationController _controller;
  @override
  void initState() {
    super.initState();

    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColorLight,
      child: Lottie.asset(
        'assets/63487-programming-computer.json',
        // animate: false
        // 'assets/108333-coding.json',
        // controller: _controller,
        // onLoaded: (composition) {
        //   // Configure the AnimationController with the duration of the
        //   // Lottie file and start the animation.

        //   print("composition.duration ${composition.duration}");
        //   // _controller
        //   //   ..duration = composition.duration
        //   //   ..forward();
        // },
      ),
    );
  }
}
