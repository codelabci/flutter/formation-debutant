import 'package:flutter/material.dart';
import 'package:formation_flutter_1/helpers/constant.dart';
import 'package:formation_flutter_1/helpers/utilities.dart';
import 'package:formation_flutter_1/models/user.dart';
import 'package:formation_flutter_1/pages/users_page.dart';

class LoginPage2 extends StatefulWidget {
  User? userToEdit;
  LoginPage2({Key? key, this.userToEdit}) : super(key: key);

  @override
  State<LoginPage2> createState() => _LoginPage2State();
}

class _LoginPage2State extends State<LoginPage2> {
  final _formKey = GlobalKey<FormState>();

  User dto = User(
    id: Utilities.getUniqueKey(),
    name: "",
    phone: "",
    password: "",
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset:
            false, // empeche les widgets de se redemensionner
        // appBar: AppBar(
        //   title: const Text("Login"),
        // ),
        // backgroundColor: Colors.transparent,
        body: Container(
          // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
          color: Theme.of(context).primaryColorLight.withOpacity(0.5),
          child: Center(
            child: SingleChildScrollView(
                reverse:
                    true, // Pour inverser le sens de scrolling par defaut (cad du bas vers le haut)
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: _formKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Column(
                      children: [
                        const Text("Welcome Back",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            )),
                        const SizedBox(height: 20),
                        TextFormField(
                          initialValue: widget.userToEdit?.email,
                          decoration: InputDecoration(
                            fillColor: Colors.white60,
                            filled: true,
                            // border: null,
                            // focusedBorder: UnderlineInputBorder(),
                            border: InputBorder.none,

                            hintText: "Email",
                            // enabled: false,
                            // suffixIcon: Icon(Icons.search),
                            prefixIcon: Icon(Icons.email),
                            iconColor: Colors.green,
                            // prefixIconColor: Colors.red,

                            icon: Icon(Icons.email),
                            // labelText: "Telephone *",
                            // border: OutlineInputBorder(
                            //     borderRadius:
                            //         BorderRadius.circular(valBorderRaduis)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return msgChampRequis;
                            }
                            return null;
                          },
                          keyboardType: TextInputType.number,
                          onChanged: (value) {},
                          onSaved: (value) {
                            dto.phone = value ?? "";
                          },
                        ),
                        const SizedBox(height: valHeigthSizeBoxForm),
                        TextFormField(
                          decoration: InputDecoration(
                            fillColor: Colors.white60,
                            filled: true,
                            prefixIcon: Icon(Icons.lock),
                            suffixIcon: Icon(Icons.remove_red_eye),
                            // labelText: "Password *",
                            hintText: "Password",
                            border: InputBorder.none,
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return msgChampRequis;
                            }
                            return null;
                          },
                          onSaved: (value) {
                            dto.password = value ?? "";
                          },
                        ),
                        const SizedBox(height: valHeigthSizeBoxForm),
                        const Text("Forgot Password ?",
                            style: TextStyle(
                                fontWeight: FontWeight.w100, fontSize: 40)),
                        ElevatedButton(
                          onPressed: submit,
                          child: const Text('Send'),
                        ),
                        const Text("I haven't an account",
                            style: TextStyle(
                                fontWeight: FontWeight.w100, fontSize: 40)),
                        Utilities.getWidgetForTopKewboard(context)
                      ],
                    ),
                  ),
                )),
          ),
        ));
  }

  submit() {
    // print(_formKey.currentState!.toString());
    if (_formKey.currentState!.validate()) {
      // If the form is valid, display a snackbar. In the real world,
      // you'd often call a server or save the information in a database.

// execute toutes les methodes save des TextFormEdit
      _formKey.currentState!.save();

      _formKey.toString();
      // User existingUser = defaultDatasUser.contains(
      //     (element) => element.name == dto.name && element.phone == dto.phone);

      print("dto $dto");

      bool existingUser = false;

      for (var element in defaultDatasUser) {
        if (element.password == dto.password && element.phone == dto.phone) {
          existingUser = true;
          break;
        }
      }
      if (existingUser) {
        Navigator.of(context).pop();
        Utilities.toast(context: context, message: "Connexion reussie !!!");

        Utilities.navigatorPush(
          context: context,
          widget: UsersPage(
            userEdited: dto,
          ),
        );
      } else {
        Utilities.toast(context: context, message: "Connexion echouée !!!");
      }

      // quelques indication sur la navigation
    } else {}
  }
}
