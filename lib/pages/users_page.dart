import 'package:flutter/material.dart';
import 'package:formation_flutter_1/helpers/constant.dart';
import 'package:formation_flutter_1/helpers/repository.dart';
import 'package:formation_flutter_1/helpers/utilities.dart';
import 'package:formation_flutter_1/models/user.dart';
import 'package:formation_flutter_1/pages/edit_user_page.dart';
import 'package:formation_flutter_1/widgets/drawer_widget.dart';

class UsersPage extends StatefulWidget {
  UsersPage({Key? key, this.userEdited, this.isDatabaseData = false})
      : super(key: key);
  User? userEdited;
  bool isDatabaseData;

  @override
  State<UsersPage> createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  bool _isSearch = false;
  @override
  void initState() {
    Utilities.begin("initState UsersPage");
    super.initState();
    updateDefaultDatasUser();
  }

  TextEditingController _searchQuery = TextEditingController();
  String searchQuery = "Search query";

  // les bonnes manieres de copier et pour le clone penser le definir
  // List<User> datasUser = [...defaultDatasUser]; //ou defaultDatasUser..toList();
  List<User> datasUser = []; //ou defaultDatasUser..toList();

  @override
  Widget build(BuildContext context) {
    Utilities.begin("build UsersPage");

    return Scaffold(
      appBar: widget.isDatabaseData
          ? null
          : AppBar(
              leading: _isSearch ? BackButton(onPressed: resetAppBar) : null,
              title: _isSearch ? _buildSearchField() : const Text("Accueil"),
              actions: [
                _isSearch
                    ? IconButton(
                        onPressed: () {
                          if (_searchQuery.text.isEmpty) {
                            resetAppBar();
                          } else {
                            _searchQuery.clear();
                            updateSearchQuery("");
                          }
                        },
                        icon: const Icon(Icons.clear_all_rounded),
                      )
                    : IconButton(
                        // onPressed: () => searchUser(name: ""),
                        onPressed: () => setState(() {
                          _isSearch = true;
                        }),
                        icon: const Icon(Icons.search),
                      )
              ],
            ),
      drawer: DrawerWidget(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // pour les debordements
              const Flexible(
                child: Text(
                  "Liste des utilisateurs",
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 25,
                      decoration: TextDecoration.underline),
                ),
              ),
              ElevatedButton(
                onPressed: addUser,
                child: const Text("Ajouter"),
              ),
            ],
          ),
          // Expanded pour etendre le reste de l'ecran afin d'eviter l'erreur Vertical viewport was given unbounded height
          const SizedBox(height: 10),
          Expanded(
            child: ListView.separated(
                itemBuilder: (context, index) {
                  var currentUser = datasUser[index];
                  return Card(
                    color: currentUser.isActive == 0
                        ? Colors.green.shade50
                        : Colors.red.shade50,
                    child: ListTile(
                      onTap: () => deleteUser(user: currentUser),
                      title: Text(currentUser.name +
                          (currentUser.firstName != null
                              ? " ${currentUser.firstName}"
                              : "")),
                      subtitle: Text(
                          "${currentUser.phone} / ${currentUser.email ?? msgAucunMail}"),
                      leading: currentUser.profil != null
                          ? Image.asset(currentUser.profil!)
                          : const Icon(Icons.image),
                      trailing: IconButton(
                          onPressed: () => editUser(user: currentUser),
                          icon: const Icon(Icons.edit)),
                    ),
                  );
                },
                separatorBuilder: (context, index) => const Divider(),
                // separatorBuilder: (context, index) => Container(),
                itemCount: datasUser.length),
          ),
        ]),
      ),
    );
  }

  resetAppBar() {
    _isSearch = false;
    datasUser = defaultDatasUser.toList();
    setState(() {});
  }

  deleteUser({required User user}) {
    print(":::: deleteUser ::::");

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          // shape: getDefaultShape(),
          title: const Text("Boite de dialogue"),
          content: const Text(
            "Etes vous sûr de supprimer cet user ?",
          ),
          actions: <Widget>[
            TextButton(
              child: Text("Non"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text("Oui"),
              onPressed: () {
                defaultDatasUser.remove(user);
                datasUser = [...defaultDatasUser];
                setState(() {});
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  addUser() {
    print(":::: addUser ::::");

    Utilities.navigatorPush(
        context: context,
        widget: EditUserPage(
          isDatabaseData: widget.isDatabaseData,
        ));
  }

  updateDefaultDatasUser() async {
    if (widget.isDatabaseData) {
      datasUser = [];
      List<Map<String, dynamic>> mapDatasUser = await SQLHelper.getUsers();
      for (var map in mapDatasUser) {
        datasUser.add(User.fromMap(map));
      }
      setState(() {});
    } else {
      if (widget.userEdited != null) {
        int currentIndex = defaultDatasUser
            .indexWhere((element) => element.id == widget.userEdited!.id);
        if (currentIndex >= 0) {
          defaultDatasUser[currentIndex] = widget.userEdited!;
        } else {
          defaultDatasUser.add(widget.userEdited!);
        }
        datasUser = [...defaultDatasUser];
        setState(() {});
      }
      datasUser = [...defaultDatasUser];
    }
  }

  editUser({required User user}) {
    print(":::: editUser ::::");
    Utilities.navigatorPush(
      context: context,
      widget: EditUserPage(
        userToEdit: user,
        isDatabaseData: widget.isDatabaseData,
      ),
    );
  }

  searchUser({required String name}) {
    print(":::: searchUser ::::");

    // datasUser.
  }

  Widget _buildSearchField() {
    return TextField(
      controller: _searchQuery,
      autofocus: true,
      decoration: const InputDecoration(
        hintText: 'Search...',
        border: InputBorder.none,
        hintStyle: TextStyle(color: Colors.white30),
      ),
      style: const TextStyle(color: Colors.white, fontSize: 16.0),
      onChanged: updateSearchQuery,
    );
  }

  void updateSearchQuery(String newQuery) {
    if (newQuery.isEmpty) {
      // print("search test " + test
      //localDatasCartes = datasCartes;
      datasUser = defaultDatasUser.toList();
      print("je suis vide query " + newQuery);
    } else {
      String newQueryToCompare = newQuery.toUpperCase();
      Iterable<User> test = defaultDatasUser.where(
          (element) => element.name.toUpperCase().contains(newQueryToCompare));
      if (test.isNotEmpty) {
        //localDatasCartes = test.toList();
        datasUser = test.toList();
      } else {
        datasUser = [];
      }
      print(test.toList().length);
    }

    setState(() {
      searchQuery = newQuery;
    });
  }
}
