import 'package:flutter/material.dart';
import 'package:formation_flutter_1/helpers/constant.dart';
import 'package:formation_flutter_1/helpers/repository.dart';
import 'package:formation_flutter_1/helpers/utilities.dart';
import 'package:formation_flutter_1/models/user.dart';
import 'package:formation_flutter_1/pages/database.dart';
import 'package:formation_flutter_1/pages/users_page.dart';

class EditUserPage extends StatefulWidget {
  User? userToEdit;
  bool isDatabaseData;
  EditUserPage({Key? key, this.userToEdit, this.isDatabaseData = false})
      : super(key: key);

  @override
  State<EditUserPage> createState() => _EditUserPageState();
}

class _EditUserPageState extends State<EditUserPage> {
  final _formKey = GlobalKey<FormState>();

  late User dto;

  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    dto = widget.userToEdit ??
        User(
          id: Utilities.getUniqueKey(),
          name: "",
          phone: "",
          password: "",
        );
    _controller.text = 'Tall';
  }

  @override
  Widget build(BuildContext context) {
    _controller.text = 'Tall';

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: widget.isDatabaseData
          ? null
          : AppBar(
              title: Text(
                  widget.userToEdit != null ? 'Editer user' : 'Ajouter user'),
            ),
      body: SingleChildScrollView(
        reverse: true,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(height: 10),
                TextFormField(
                  controller: _controller,
                  decoration: InputDecoration(
                    labelText: "Name *",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                  // initialValue: widget.userToEdit?.name,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return msgChampRequis;
                    }
                    return null;
                  },
                  onSaved: (value) {
                    print("name $value");
                    dto.name = value ?? "";
                  },
                ),
                const SizedBox(height: valHeigthSizeBoxForm),
                TextFormField(
                  initialValue: dto.firstName,
                  decoration: InputDecoration(
                    labelText: "First Name",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                  onSaved: (value) {
                    dto.firstName = value;
                  },
                ),
                const SizedBox(height: valHeigthSizeBoxForm),
                TextFormField(
                  initialValue: dto.phone,
                  decoration: InputDecoration(
                    labelText: "Telephone *",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return msgChampRequis;
                    }
                    return null;
                  },
                  onSaved: (value) {
                    dto.phone = value ?? "";
                  },
                ),
                const SizedBox(height: valHeigthSizeBoxForm),
                TextFormField(
                  initialValue: dto.email,
                  decoration: InputDecoration(
                    labelText: "Email",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                  onSaved: (value) {
                    dto.email = value;
                  },
                ),
                const SizedBox(height: valHeigthSizeBoxForm),
                TextFormField(
                  initialValue: dto.password,
                  decoration: InputDecoration(
                    labelText: "Password *",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4)),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return msgChampRequis;
                    }
                    return null;
                  },
                  onSaved: (value) {
                    dto.password = value ?? "";
                  },
                ),
                const SizedBox(height: valHeigthSizeBoxForm),
                Checkbox(
                  value: dto.isActive == 0,
                  onChanged: (value) {
                    dto.isActive = value != null && value ? 1 : 0;
                    setState(() {});
                  },
                ),
                ElevatedButton(
                  onPressed: submit,
                  child:
                      Text(widget.userToEdit != null ? 'Modifer' : 'Ajouter'),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  initForm() {}

  submit() async {
    // _formKey.currentState!.save();

    SQLHelper.db();

    if (_formKey.currentState!.validate()) {
      // If the form is valid, display a snackbar. In the real world,
      // you'd often call a server or save the information in a database.

      // execute toutes les methodes save des TextFormEdit
      _formKey.currentState!.save();
      if (widget.isDatabaseData) {
        widget.userToEdit != null
            ? await SQLHelper.updateUser(dto)
            : await SQLHelper.createUser(dto);
      }
      defaultDatasUser.add(dto);

      Utilities.toast(context: context, message: "Processing Data");
      Navigator.of(context).pop();
      Utilities.navigatorPush(
        context: context,
        widget: widget.isDatabaseData
            ? CrudDatabase()
            : UsersPage(
                userEdited: dto,
                isDatabaseData: widget.isDatabaseData,
              ),
      );
      // quelques indication sur la navigation
    } else {}
  }
}
