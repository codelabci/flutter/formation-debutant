import 'package:flutter/material.dart';
import 'package:formation_flutter_1/helpers/constant.dart';
import 'package:formation_flutter_1/helpers/utilities.dart';
import 'package:formation_flutter_1/models/user.dart';
import 'package:formation_flutter_1/pages/users_page.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  User dto = User(
    id: Utilities.getUniqueKey(),
    name: "",
    phone: "",
    password: "",
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset:
            false, // empeche les widgets de se redemensionner
        // appBar: AppBar(
        //   title: const Text("Login"),
        // ),
        // backgroundColor: Colors.transparent,
        body: Container(
          // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
          color: Theme.of(context).primaryColorLight.withOpacity(0.5),
          child: Center(
            child: SingleChildScrollView(
                reverse:
                    true, // Pour inverser le sens de scrolling par defaut (cad du bas vers le haut)
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.2,
                          child: Image.asset(
                              'assets/63487-programming-computer.gif',
                              fit: BoxFit.cover),
                        ),
                        const SizedBox(height: 30),
                        const Text("Connexion",
                            style: TextStyle(
                                fontWeight: FontWeight.w100, fontSize: 40)),
                        const SizedBox(height: 30),
                        TextFormField(
                          decoration: InputDecoration(
                            fillColor: Colors.white60,
                            filled: true,
                            labelText: "Telephone *",
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(valBorderRaduis)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return msgChampRequis;
                            }
                            return null;
                          },
                          onSaved: (value) {
                            dto.phone = value ?? "";
                          },
                        ),
                        const SizedBox(height: valHeigthSizeBoxForm),
                        TextFormField(
                          decoration: InputDecoration(
                            fillColor: Colors.white60,
                            filled: true,
                            labelText: "Password *",
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(valBorderRaduis)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return msgChampRequis;
                            }
                            return null;
                          },
                          onSaved: (value) {
                            dto.password = value ?? "";
                          },
                        ),
                        const SizedBox(height: valHeigthSizeBoxForm),
                        ElevatedButton(
                          onPressed: submit,
                          child: const Text('Send'),
                        ),
                        Utilities.getWidgetForTopKewboard(context)
                      ],
                    ),
                  ),
                )),
          ),
        ));
  }

  submit() {
    if (_formKey.currentState!.validate()) {
      // If the form is valid, display a snackbar. In the real world,
      // you'd often call a server or save the information in a database.

// execute toutes les methodes save des TextFormEdit
      _formKey.currentState!.save();

      // User existingUser = defaultDatasUser.contains(
      //     (element) => element.name == dto.name && element.phone == dto.phone);

      print("dto $dto");

      bool existingUser = false;

      for (var element in defaultDatasUser) {
        if (element.password == dto.password && element.phone == dto.phone) {
          existingUser = true;
          break;
        }
      }
      if (existingUser) {
        Navigator.of(context).pop();
        Utilities.toast(context: context, message: "Connexion reussie !!!");

        Utilities.navigatorPush(
          context: context,
          widget: UsersPage(
            userEdited: dto,
          ),
        );
      } else {
        Utilities.toast(context: context, message: "Connexion echouée !!!");
      }

      // quelques indication sur la navigation
    } else {}
  }
}
