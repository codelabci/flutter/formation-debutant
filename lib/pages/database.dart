import 'package:flutter/material.dart';
import 'package:formation_flutter_1/pages/edit_user_page.dart';
import 'package:formation_flutter_1/pages/users_page.dart';

class CrudDatabase extends StatefulWidget {
  const CrudDatabase({super.key});

  @override
  State<CrudDatabase> createState() => _CrudDatabaseState();
}

class _CrudDatabaseState extends State<CrudDatabase> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Base de données en locale"),
            automaticallyImplyLeading: false, // remove callback button
            bottom: const TabBar(
              tabs: <Widget>[
                Tab(
                  icon: Icon(Icons.edit),
                  text: "Editer user",
                ),
                Tab(
                  icon: Icon(Icons.list),
                  text: "Users",
                ),
              ],
            ),
          ),
          body: TabBarView(
            // controller: tabController,
            children: <Widget>[
              EditUserPage(
                isDatabaseData: true,
              ),
              UsersPage(
                isDatabaseData: true,
              ),
            ],
          ),
        ));
  }
}
