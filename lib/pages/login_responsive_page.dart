import 'package:flutter/material.dart';
import 'package:formation_flutter_1/helpers/constant.dart';
import 'package:formation_flutter_1/helpers/utilities.dart';
import 'package:formation_flutter_1/models/user.dart';
import 'package:formation_flutter_1/pages/users_page.dart';

class LoginResponsivePage extends StatefulWidget {
  LoginResponsivePage({Key? key}) : super(key: key);

  @override
  State<LoginResponsivePage> createState() => _LoginResponsivePageState();
}

class _LoginResponsivePageState extends State<LoginResponsivePage> {
  User dto = User(
    id: Utilities.getUniqueKey(),
    name: "",
    phone: "",
    password: "",
  );
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        late Widget currentWidget;

        double maxWidth = constraints.maxWidth;
        // double minWidth = constraints.minWidth;

        if (maxWidth <= valScreenMobileDevices) {
          currentWidget = LoginMobilePage(dto: dto, formKey: _formKey);
        } else if (maxWidth <= valScreenTabletsAndIpads) {
          currentWidget = LoginTabletPage(dto: dto, formKey: _formKey);
        } else if (maxWidth <= valScreenLatopsAndSmallScreens) {
          currentWidget = LoginDesktopPage(dto: dto, formKey: _formKey);
        } else if (maxWidth <= valScreenDesktopsAndLargeScreens) {
          currentWidget = LoginDesktopPage(dto: dto, formKey: _formKey);
        } else if (maxWidth > valScreenDesktopsAndLargeScreens) {
          currentWidget = LoginDesktopPage(dto: dto, formKey: _formKey);
        }

        return currentWidget;
      },
    );
  }
}

submit(
    {required BuildContext context,
    required GlobalKey<FormState> formKey,
    required User dto}) {
  if (formKey.currentState!.validate()) {
    // If the form is valid, display a snackbar. In the real world,
    // you'd often call a server or save the information in a database.

// execute toutes les methodes save des TextFormEdit
    formKey.currentState!.save();
    // User existingUser = defaultDatasUser.contains(
    //     (element) => element.name == dto.name && element.phone == dto.phone);

    print("dto $dto");

    bool existingUser = false;

    for (var element in defaultDatasUser) {
      if (element.password == dto.password && element.phone == dto.phone) {
        existingUser = true;
        break;
      }
    }
    if (existingUser) {
      Navigator.of(context).pop();
      Utilities.toast(context: context, message: "Connexion reussie !!!");

      Utilities.navigatorPush(
        context: context,
        widget: UsersPage(
          userEdited: dto,
        ),
      );
    } else {
      Utilities.toast(context: context, message: "Connexion echouée !!!");
    }

    // quelques indication sur la navigation
  } else {}
}

class LoginTabletPage extends StatefulWidget {
  User dto;
  GlobalKey<FormState> formKey;
  LoginTabletPage({Key? key, required this.dto, required this.formKey})
      : super(key: key);

  @override
  State<LoginTabletPage> createState() => _LoginTabletPageState();
}

class _LoginTabletPageState extends State<LoginTabletPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("Login"),
      // ),
      // backgroundColor: Colors.transparent,
      body: Row(
        children: [
          Expanded(
            child: Container(
              // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
              // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/108333-coding.png'),
                    fit: BoxFit.cover),
              ),
            ),
          ),
          Container(
            // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
            color: Theme.of(context).primaryColorLight.withOpacity(0.5),
            constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.3),

            child: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: widget.formKey,
                    child: Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.2,
                          child: Image.asset(
                              'assets/63487-programming-computer.gif',
                              fit: BoxFit.cover),
                        ),
                        const SizedBox(height: 30),
                        const Text("Connexion",
                            style: TextStyle(
                                fontWeight: FontWeight.w100, fontSize: 40)),
                        const SizedBox(height: 30),
                        TextFormField(
                          decoration: InputDecoration(
                            fillColor: Colors.white60,
                            filled: true,
                            labelText: "Telephone *",
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(valBorderRaduis)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return msgChampRequis;
                            }
                            return null;
                          },
                          onSaved: (value) {
                            widget.dto.phone = value ?? "";
                          },
                        ),
                        const SizedBox(height: valHeigthSizeBoxForm),
                        TextFormField(
                          decoration: InputDecoration(
                            fillColor: Colors.white60,
                            filled: true,
                            labelText: "Password *",
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(valBorderRaduis)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return msgChampRequis;
                            }
                            return null;
                          },
                          onSaved: (value) {
                            widget.dto.password = value ?? "";
                          },
                        ),
                        ElevatedButton(
                          onPressed: () => submit(
                              context: context,
                              formKey: widget.formKey,
                              dto: widget.dto),
                          child: const Text('Send'),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class LoginDesktopPage extends StatefulWidget {
  User dto;
  GlobalKey<FormState> formKey;
  LoginDesktopPage({Key? key, required this.dto, required this.formKey})
      : super(key: key);

  @override
  State<LoginDesktopPage> createState() => _LoginDesktopPageState();
}

class _LoginDesktopPageState extends State<LoginDesktopPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("Login"),
      // ),
      // backgroundColor: Colors.transparent,
      body: Row(
        children: [
          Expanded(
            child: Container(
              // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
              // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/108333-coding.gif'),
                    fit: BoxFit.cover),
              ),
            ),
          ),
          Container(
            // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
            color: Theme.of(context).primaryColorLight.withOpacity(0.5),
            constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.5),

            child: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: widget.formKey,
                    child: Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.2,
                          child: Image.asset(
                              'assets/63487-programming-computer.gif',
                              fit: BoxFit.cover),
                        ),
                        const SizedBox(height: 30),
                        const Text("Connexion",
                            style: TextStyle(
                                fontWeight: FontWeight.w100, fontSize: 40)),
                        const SizedBox(height: 30),
                        TextFormField(
                          decoration: InputDecoration(
                            fillColor: Colors.white60,
                            filled: true,
                            labelText: "Telephone *",
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(valBorderRaduis)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return msgChampRequis;
                            }
                            return null;
                          },
                          onSaved: (value) {
                            widget.dto.phone = value ?? "";
                          },
                        ),
                        const SizedBox(height: valHeigthSizeBoxForm),
                        TextFormField(
                          decoration: InputDecoration(
                            fillColor: Colors.white60,
                            filled: true,
                            labelText: "Password *",
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(valBorderRaduis)),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return msgChampRequis;
                            }
                            return null;
                          },
                          onSaved: (value) {
                            widget.dto.password = value ?? "";
                          },
                        ),
                        ElevatedButton(
                          onPressed: () => submit(
                              context: context,
                              formKey: widget.formKey,
                              dto: widget.dto),
                          child: const Text('Send'),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
    ;
  }
}

class LoginMobilePage extends StatefulWidget {
  User dto;
  GlobalKey<FormState> formKey;
  LoginMobilePage({Key? key, required this.dto, required this.formKey})
      : super(key: key);

  @override
  State<LoginMobilePage> createState() => _LoginMobilePageState();
}

class _LoginMobilePageState extends State<LoginMobilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("Login"),
      // ),
      // backgroundColor: Colors.transparent,
      body: Container(
        // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
        // color: Theme.of(context).primaryColorLight.withOpacity(0.5),
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/register.png'), fit: BoxFit.cover),
        ),
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                key: widget.formKey,
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.2,
                      child: Image.asset(
                          'assets/63487-programming-computer.gif',
                          fit: BoxFit.cover),
                    ),
                    const SizedBox(height: 30),
                    const Text("Connexion",
                        style: TextStyle(
                            fontWeight: FontWeight.w100, fontSize: 40)),
                    const SizedBox(height: 30),
                    TextFormField(
                      decoration: InputDecoration(
                        fillColor: Colors.white60,
                        filled: true,
                        labelText: "Telephone *",
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(valBorderRaduis)),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return msgChampRequis;
                        }
                        return null;
                      },
                      onSaved: (value) {
                        widget.dto.phone = value ?? "";
                      },
                    ),
                    const SizedBox(height: valHeigthSizeBoxForm),
                    TextFormField(
                      decoration: InputDecoration(
                        fillColor: Colors.white60,
                        filled: true,
                        labelText: "Password *",
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(valBorderRaduis)),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return msgChampRequis;
                        }
                        return null;
                      },
                      onSaved: (value) {
                        widget.dto.password = value ?? "";
                      },
                    ),
                    ElevatedButton(
                      onPressed: () => submit(
                          context: context,
                          formKey: widget.formKey,
                          dto: widget.dto),
                      child: const Text('Send'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
    ;
  }
}
