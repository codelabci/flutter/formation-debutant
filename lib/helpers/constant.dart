import 'package:flutter/material.dart';
import 'package:formation_flutter_1/helpers/utilities.dart';
import 'package:formation_flutter_1/models/user.dart';

const datasPopupMenuButton = [
  "Anti-Spams activé",
  "Gérer vos favoris",
  "Paramètres",
  "Actualités Orange Téléphone",
  "Aide",
];

const lbArchives = "Archivées";
const lbSpam = "Spam et coversations bloques";
const lbToutMarque = "Tout marquer comme lu";
const lbParametres = "Paramètres";
const lbAide = "Aide et commentaires";
const datasPopupMenuButtonMessage = [
  lbToutMarque,
  lbArchives,
  lbSpam,
  lbParametres,
  lbAide,
];

const datasContact = [
  "Adama Moov",
  "Maman",
  "Kone seriba",
  "Aboubacar sidik",
  "Macon konate",
];
const defaultLastContent =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown";

const Color red = Color(0xFFFF0000);
const Color black = Color(0xFF000000);
const Color yellow = Color.fromRGBO(255, 235, 59, 1);
const datasAllFileMessage = [
  {
    'name': '+454',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': red,
  },
  {
    'name': 'Orange et Moi',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': yellow,
  },
  {
    'name': 'MTN BonPlan',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.blue,
  },
  {
    'name': '404',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.orange,
  },
  {
    'name': 'Touré zié',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': Colors.indigo,
  },
  {
    'name': 'Coulibaly zié',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.red,
  },
  {
    'name': 'Diallo zié',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.green,
  },
  {
    'name': 'Tall zié',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.black,
  },
  {
    'name': 'Seriba',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': Colors.green,
  },
  {
    'name': 'Moov Africa',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.yellow,
  },
  {
    'name': 'Mardi Bonus',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': true,
    'color': Colors.blueGrey,
  },
  {
    'name': '0595054040',
    "lastContent": defaultLastContent,
    "heure": "08:30",
    'isShow': false,
    'color': Colors.grey,
  },
];

final datasContentFileMessage = [
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": true,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Hier",
  },
  {
    "content": defaultLastContent,
    "isMe": false,
    "jour": "Aujourd'hui",
  },
  {
    "content": defaultLastContent,
    "isMe": true,
    "jour": "Aujourd'hui",
  },
  {
    "content": defaultLastContent,
    "isMe": true,
    "jour": "Aujourd'hui",
  },
];

var data = [
  {
    "name": defaultLastContent,
    "sousName": true,
    "elementForGroupBy": "Mise a jour recente",
  },
  {
    "name": defaultLastContent,
    "sousName": true,
    "elementForGroupBy": "Mise a jour recente",
  },
  {
    "name": defaultLastContent,
    "sousName": true,
    "elementForGroupBy": "Mise a jour recente",
  },
  {
    "name": defaultLastContent,
    "sousName": true,
    "elementForGroupBy": "Mise a jour recente",
  },
  {
    "name": defaultLastContent,
    "sousName": true,
    "elementForGroupBy": "Mise a jour vues",
  },
  {
    "name": defaultLastContent,
    "sousName": true,
    "elementForGroupBy": "Mise a jour vues",
  },
  {
    "name": defaultLastContent,
    "sousName": true,
    "elementForGroupBy": "Mise a jour vues",
  },
  {
    "name": defaultLastContent,
    "sousName": true,
    "elementForGroupBy": "Mise a jour vues",
  },
];

const datasCurrentCall = [
  {
    'name': 'Touré zié',
    "phone": "0595054040",
    "heure": "08:30",
    'type': "Principal",
    'sim': 1,
    'group': "Aujourd'hui"
  },
  {
    'name': 'Koné irié',
    "phone": "0585054040",
    "heure": "08:20",
    'type': "Portable",
    'sim': 2,
    'group': "Aujourd'hui"
  },
  {
    'name': 'Kouassi seriba',
    "phone": "0575054040",
    "heure": "08:02",
    'type': "Principal",
    'sim': 1,
    'group': "Aujourd'hui"
  },
  {
    'name': 'Kouame',
    "phone": "0565054040",
    "heure": "08:01",
    'type': "Principal",
    'sim': 1,
    'group': "Aujourd'hui"
  },
  {
    'name': 'Mathias',
    "phone": "0555054040",
    "heure": "08:00",
    'type': "Portable",
    'sim': 2,
    'group': "Hier"
  },
  {
    'name': 'Assetou',
    "phone": "0545054040",
    "heure": "07:02",
    'type': "Portable",
    'sim': 1,
    'group': "Hier"
  },
  {
    'name': 'Aminata',
    "phone": "0535054040",
    "heure": "08:02",
    'type': "Portable",
    'sim': 2,
    'group': "Avant-hier"
  },
  {
    'name': 'Yelle',
    "phone": "0525054040",
    "heure": "08:00",
    'type': "Portable",
    'sim': 1,
    'group': "Avant-hier"
  },
  {
    'name': 'Kiki',
    "phone": "0515054040",
    "heure": "07:02",
    'type': "Portable",
    'sim': 2,
    'group': "Avant-hier"
  },
];
List<User> defaultDatasUser = [
  User(
    id: Utilities.getUniqueKey(),
    name: "Oumar",
    firstName: "coulibaly",
    phone: "0755504040",
    password: "1234",
    isActive: 1,
  ),
  User(
    id: Utilities.getUniqueKey(),
    name: "Souleymane",
    phone: "0755504040",
    password: "1234",
  ),
  User(
    id: Utilities.getUniqueKey(),
    name: "Ali",
    firstName: "Soro",
    phone: "0555504040",
    password: "1234",
    email: "ali.soro@codelab.ci",
    isActive: 1,
  ),
];

const msgChampRequis = "Champ requis";
const msgAucunMail = 'Aucun email';

const valRaduis = 10.0;

const raduis = Radius.circular(valRaduis);
const valHeigthSizeBoxForm = 10.0;
const valBorderRaduis = 30.0;
const valSizeIcon = 15.0;
const valWidthSizedbox = 5.0;

const valScreenMobileDevices = 480;
const valScreenTabletsAndIpads = 768;
const valScreenLatopsAndSmallScreens = 1024;
const valScreenDesktopsAndLargeScreens = 1200;

const apiUrl = "https://api.github.com/search/users?q=";

//https://api.github.com/
