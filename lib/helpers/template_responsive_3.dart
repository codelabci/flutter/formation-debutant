import 'package:flutter/material.dart';

const int largeScreenSize = 1366;
const int mediumScreenSize = 768;
const int smallSceenSize = 360;
const int customScreenSize = 1100;

const screenMobileDevices = 480;
const screenTabletsAndIpads = 768;
const screenLatopsAndSmallScreens = 1024;
const screenDesktopsAndLargeScreens = 1200;
// const  screenExtraLargeAndTV = 1201 ;

// 320px — 480px: Mobile devices
// 481px — 768px: iPads, Tablets
// 769px — 1024px: Small screens, laptops
// 1025px — 1200px: Desktops, large screens
// 1201px and more —  Extra large screens, TV

class ResponsiveWidget3 extends StatelessWidget {
  // the custom screen size is specific to this project
  final Widget largeScreen;
  final Widget? mediumScreen;
  final Widget? smallScreen;
  final Widget? customScreen;

  const ResponsiveWidget3({
    Key? key,
    required this.largeScreen,
    this.mediumScreen,
    this.smallScreen,
    this.customScreen,
  }) : super(key: key);

  static bool isScreenMobileDevices(BuildContext context) {
    return MediaQuery.of(context).size.width <= screenMobileDevices;
  }

  static bool isScreenTabletsAndIpads(BuildContext context) {
    return MediaQuery.of(context).size.width > screenMobileDevices &&
        MediaQuery.of(context).size.width <= screenTabletsAndIpads;
  }

  static bool isScreenLatopsAndSmallScreens(BuildContext context) {
    return MediaQuery.of(context).size.width > screenTabletsAndIpads &&
        MediaQuery.of(context).size.width <= screenLatopsAndSmallScreens;
  }

  static bool isScreenDesktopsAndLargeScreens(BuildContext context) {
    return MediaQuery.of(context).size.width > screenLatopsAndSmallScreens &&
        MediaQuery.of(context).size.width <= screenDesktopsAndLargeScreens;
  }

  static bool isScreenExtraLargeAndTV(BuildContext context) {
    return MediaQuery.of(context).size.width > screenDesktopsAndLargeScreens;
  }

  static bool isSmallScreen(BuildContext context) {
    return MediaQuery.of(context).size.width < mediumScreenSize;
  }

  static bool isMediumScreen(BuildContext context) {
    return MediaQuery.of(context).size.width >= mediumScreenSize &&
        MediaQuery.of(context).size.width < largeScreenSize;
  }

  static bool isLargeScreen(BuildContext context) {
    return MediaQuery.of(context).size.width > largeScreenSize;
  }

  static bool isCustomSize(BuildContext context) {
    return MediaQuery.of(context).size.width <= customScreenSize &&
        MediaQuery.of(context).size.width >= mediumScreenSize;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth >= largeScreenSize) {
          return largeScreen;
        } else if (constraints.maxWidth < largeScreenSize &&
            constraints.maxWidth >= mediumScreenSize) {
          return mediumScreen ?? largeScreen;
        } else {
          return smallScreen ?? largeScreen;
        }
      },
    );
  }
}
