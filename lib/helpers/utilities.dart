import 'package:flutter/material.dart';
import 'package:formation_flutter_1/main.dart';

class Utilities {
  static begin(element) {
    print("*********************BEGIN $element*********************");
  }

  static log(element) {
    print("*********************LOG $element*********************");
  }

  static end(element) {
    print("*********************END $element*********************");
  }

  static toast({required BuildContext context, String message = "Selected "}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  static getWidgetForTopKewboard(BuildContext context) {
    return Padding(
      // padding:
      //     EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      padding: MediaQuery.of(context).viewInsets,
    );
  }

  static navigatorPush({required BuildContext context, dynamic widget}) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => widget),
    );
  }

  static urlEncode(String value) {
    return Uri.encodeComponent(value);
  }

  static getDefaultShape(
          {double topRight = 10.0,
          double topLeft = 10.0,
          double bottomLeft = 10.0,
          double bottomRight = 10.0,
          BorderSide side = BorderSide.none}) =>
      RoundedRectangleBorder(
        side: side,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft),
          topRight: Radius.circular(topRight),
          bottomLeft: Radius.circular(bottomLeft),
          bottomRight: Radius.circular(bottomRight),
        ),
      );

  static resetAndOpenPage({required dynamic context, dynamic view}) {
    begin("resetAndOpenPage");
    //pushNamedAndRemoveUntil
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (BuildContext context) => view ?? MyHomePage()),
        (Route<dynamic> route) => false

        //ModalRoute.withName('/'),
        );
    end("resetAndOpenPage");
  }

  static getUniqueKey() {
    return UniqueKey().toString();
  }

  static bool isIOS(BuildContext context) {
    return Theme.of(context).platform == TargetPlatform.iOS;
  }

  static isHandset(BuildContext context) {
    return (Theme.of(context).platform == TargetPlatform.iOS ||
            Theme.of(context).platform == TargetPlatform.android) &&
        MediaQuery.of(context).size.width < 700;
  }

  //320px, 480px, 760px, 960px, 1200px and 1600px.
}
