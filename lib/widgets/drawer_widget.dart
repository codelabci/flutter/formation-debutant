import 'package:flutter/material.dart';
import 'package:formation_flutter_1/helpers/utilities.dart';
import 'package:formation_flutter_1/pages/database.dart';
import 'package:formation_flutter_1/pages/get_search_by_api_page.dart';
import 'package:formation_flutter_1/pages/landing_page.dart';
import 'package:formation_flutter_1/pages/login_page.dart';
import 'package:formation_flutter_1/pages/login_page_2.dart';
import 'package:formation_flutter_1/pages/login_responsive_page.dart';
import 'package:formation_flutter_1/pages/message_app.dart';
import 'package:formation_flutter_1/pages/telephone_app_page.dart';
import 'package:formation_flutter_1/pages/test.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            currentAccountPicture:
                Image.asset('assets/63487-programming-computer.gif'),
            accountEmail: const Text(
              "formations@codelab.ci",
              style: TextStyle(fontWeight: FontWeight.w300),
            ),
            accountName: const Text(
              'Codelab CI',
              style: TextStyle(fontWeight: FontWeight.w300),
            ),
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              Theme.of(context).primaryColor,
              Theme.of(context).primaryColorLight,
              Colors.white
            ])

                //LinearGradient(colors: [ConstApp.color_app, Colors.white])
                //LinearGradient(colors: [Colors.deepOrange, Colors.white])
                //gradient: LinearGradient(colors: [MainConst.color_app_rouge, Colors.white])
                ),
          ),
          // ListTile(
          //   title: const Text('Test'),
          //   onTap: () {
          //     Utilities.toast(context: context, message: "Test App");
          //     // ordre important
          //     Navigator.pop(context); // la pile des ecrans
          //     Utilities.navigatorPush(
          //         context: context, widget: ScrollablePositionedListPage());
          //   },
          //   leading: const Icon(Icons.search),
          // ),
          ListTile(
            title: const Text('Téléphone App'),
            onTap: () {
              Utilities.toast(context: context, message: "Téléphone App");
              // ordre important
              Navigator.pop(context); // la pile des ecrans
              Utilities.navigatorPush(
                  context: context, widget: TelephoneAppPage());
            },
            leading: const Icon(Icons.phone),
          ),
          ListTile(
            title: const Text('Message App'),
            onTap: () {
              Utilities.toast(context: context, message: "Message App");
              // ordre important
              Navigator.pop(context); // la pile des ecrans
              Utilities.navigatorPush(context: context, widget: MessageApp());
            },
            leading: const Icon(Icons.phone),
          ),
          ListTile(
            title: const Text('Search by API'),
            onTap: () {
              Utilities.toast(context: context, message: "Search by API");
              // ordre important
              Navigator.pop(context); // la pile des ecrans
              Utilities.navigatorPush(
                  context: context, widget: GetSearchByApiPage());
            },
            leading: const Icon(Icons.search),
          ),

          ListTile(
            title: const Text('CRUD with Database'),
            onTap: () {
              Utilities.toast(context: context, message: "CRUD with Database");
              // ordre important
              Navigator.pop(context); // la pile des ecrans
              Utilities.navigatorPush(
                  context: context, widget: const CrudDatabase());
            },
            leading: const Icon(Icons.storage_rounded),
          ),
          ListTile(
            title: const Text('Login 1'),
            onTap: () {
              Utilities.toast(context: context, message: "Login 1");
              // ordre important
              Navigator.pop(context); // la pile des ecrans
              Utilities.navigatorPush(context: context, widget: LoginPage2());
            },
            leading: const Icon(Icons.phone),
          ),
          ListTile(
            title: const Text('Responsive page'),
            onTap: () {
              Utilities.toast(context: context, message: "Responsive page");
              Navigator.pop(context);
              Utilities.navigatorPush(
                  context: context, widget: LoginResponsivePage());
            },
            leading: const Icon(Icons.all_out),
          ),
          ListTile(
            title: const Text('Landing page'),
            onTap: () {
              Utilities.toast(context: context, message: "Landing page");
              Navigator.pop(context);
              Utilities.navigatorPush(context: context, widget: LandingPage());
            },
            leading: const Icon(Icons.rocket_launch),
          ),
          ListTile(
            title: const Text('About'),
            onTap: () {
              Utilities.toast(context: context, message: "Selected About");
              Navigator.pop(context);
            },
            leading: const Icon(Icons.info_outline_rounded),
          ),
          ListTile(
            title: const Text('Settings'),
            onTap: () {
              Utilities.toast(context: context, message: "Selected Settings");
              Navigator.pop(context);
            },
            leading: const Icon(Icons.settings_rounded),
          ),
          ListTile(
            title: const Text('Exit'),
            onTap: () {
              Utilities.toast(context: context, message: "Selected Exit");
              Navigator.pop(context);
              Utilities.resetAndOpenPage(context: context, view: LoginPage());
            },
            leading: const Icon(Icons.exit_to_app_rounded),
          ),
          ListTile(
            title: const Text('Help'),
            onTap: () {
              Utilities.toast(context: context, message: "Selected Help");
              Navigator.pop(context);
            },
            leading: const Icon(Icons.help_outline_rounded),
          ),
        ],
      ),
    );
  }
}
